import { element, by, ElementFinder } from 'protractor';

export class ClienteComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-cliente div table .btn-danger'));
    title = element.all(by.css('jhi-cliente div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class ClienteUpdatePage {
    pageTitle = element(by.id('jhi-cliente-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nombreInput = element(by.id('field_nombre'));
    sexoSelect = element(by.id('field_sexo'));
    documentoInput = element(by.id('field_documento'));
    emailInput = element(by.id('field_email'));
    telefonoInput = element(by.id('field_telefono'));
    fecNacimientoInput = element(by.id('field_fecNacimiento'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setNombreInput(nombre) {
        await this.nombreInput.sendKeys(nombre);
    }

    async getNombreInput() {
        return this.nombreInput.getAttribute('value');
    }

    async setSexoSelect(sexo) {
        await this.sexoSelect.sendKeys(sexo);
    }

    async getSexoSelect() {
        return this.sexoSelect.element(by.css('option:checked')).getText();
    }

    async sexoSelectLastOption() {
        await this.sexoSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setDocumentoInput(documento) {
        await this.documentoInput.sendKeys(documento);
    }

    async getDocumentoInput() {
        return this.documentoInput.getAttribute('value');
    }

    async setEmailInput(email) {
        await this.emailInput.sendKeys(email);
    }

    async getEmailInput() {
        return this.emailInput.getAttribute('value');
    }

    async setTelefonoInput(telefono) {
        await this.telefonoInput.sendKeys(telefono);
    }

    async getTelefonoInput() {
        return this.telefonoInput.getAttribute('value');
    }

    async setFecNacimientoInput(fecNacimiento) {
        await this.fecNacimientoInput.sendKeys(fecNacimiento);
    }

    async getFecNacimientoInput() {
        return this.fecNacimientoInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ClienteDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-cliente-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-cliente'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
