import { element, by, ElementFinder } from 'protractor';

export class ProductoComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-producto div table .btn-danger'));
    title = element.all(by.css('jhi-producto div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class ProductoUpdatePage {
    pageTitle = element(by.id('jhi-producto-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    descripcionInput = element(by.id('field_descripcion'));
    precioInput = element(by.id('field_precio'));
    tipoSelect = element(by.id('field_tipo'));
    activoInput = element(by.id('field_activo'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setDescripcionInput(descripcion) {
        await this.descripcionInput.sendKeys(descripcion);
    }

    async getDescripcionInput() {
        return this.descripcionInput.getAttribute('value');
    }

    async setPrecioInput(precio) {
        await this.precioInput.sendKeys(precio);
    }

    async getPrecioInput() {
        return this.precioInput.getAttribute('value');
    }

    async setTipoSelect(tipo) {
        await this.tipoSelect.sendKeys(tipo);
    }

    async getTipoSelect() {
        return this.tipoSelect.element(by.css('option:checked')).getText();
    }

    async tipoSelectLastOption() {
        await this.tipoSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    getActivoInput() {
        return this.activoInput;
    }
    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ProductoDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-producto-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-producto'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
