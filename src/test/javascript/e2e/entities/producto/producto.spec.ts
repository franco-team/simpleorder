/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ProductoComponentsPage, ProductoDeleteDialog, ProductoUpdatePage } from './producto.page-object';

const expect = chai.expect;

describe('Producto e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let productoUpdatePage: ProductoUpdatePage;
    let productoComponentsPage: ProductoComponentsPage;
    let productoDeleteDialog: ProductoDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Productos', async () => {
        await navBarPage.goToEntity('producto');
        productoComponentsPage = new ProductoComponentsPage();
        await browser.wait(ec.visibilityOf(productoComponentsPage.title), 5000);
        expect(await productoComponentsPage.getTitle()).to.eq('Productos');
    });

    it('should load create Producto page', async () => {
        await productoComponentsPage.clickOnCreateButton();
        productoUpdatePage = new ProductoUpdatePage();
        expect(await productoUpdatePage.getPageTitle()).to.eq('Create or edit a Producto');
        await productoUpdatePage.cancel();
    });

    it('should create and save Productos', async () => {
        const nbButtonsBeforeCreate = await productoComponentsPage.countDeleteButtons();

        await productoComponentsPage.clickOnCreateButton();
        await promise.all([
            productoUpdatePage.setDescripcionInput('descripcion'),
            productoUpdatePage.setPrecioInput('5'),
            productoUpdatePage.tipoSelectLastOption()
        ]);
        expect(await productoUpdatePage.getDescripcionInput()).to.eq('descripcion');
        expect(await productoUpdatePage.getPrecioInput()).to.eq('5');
        const selectedActivo = productoUpdatePage.getActivoInput();
        if (await selectedActivo.isSelected()) {
            await productoUpdatePage.getActivoInput().click();
            expect(await productoUpdatePage.getActivoInput().isSelected()).to.be.false;
        } else {
            await productoUpdatePage.getActivoInput().click();
            expect(await productoUpdatePage.getActivoInput().isSelected()).to.be.true;
        }
        await productoUpdatePage.save();
        expect(await productoUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await productoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Producto', async () => {
        const nbButtonsBeforeDelete = await productoComponentsPage.countDeleteButtons();
        await productoComponentsPage.clickOnLastDeleteButton();

        productoDeleteDialog = new ProductoDeleteDialog();
        expect(await productoDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Producto?');
        await productoDeleteDialog.clickOnConfirmButton();

        expect(await productoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
