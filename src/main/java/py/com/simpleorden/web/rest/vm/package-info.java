/**
 * View Models used by Spring MVC REST controllers.
 */
package py.com.simpleorden.web.rest.vm;
