package py.com.simpleorden.domain.enumeration;

/**
 * The TIPO enumeration.
 */
public enum TIPO {
    POR_KILO, POR_CANTIDAD
}
