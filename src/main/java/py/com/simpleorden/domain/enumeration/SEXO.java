package py.com.simpleorden.domain.enumeration;

/**
 * The SEXO enumeration.
 */
public enum SEXO {
    MUJER, HOMBRE
}
