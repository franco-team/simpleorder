import { NgModule } from '@angular/core';

import { SimpleordenSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [SimpleordenSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [SimpleordenSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class SimpleordenSharedCommonModule {}
