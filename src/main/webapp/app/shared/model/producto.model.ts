export const enum TIPO {
    POR_KILO = 'POR_KILO',
    POR_CANTIDAD = 'POR_CANTIDAD'
}

export interface IProducto {
    id?: number;
    descripcion?: string;
    precio?: number;
    tipo?: TIPO;
    activo?: boolean;
}

export class Producto implements IProducto {
    constructor(public id?: number, public descripcion?: string, public precio?: number, public tipo?: TIPO, public activo?: boolean) {
        this.activo = this.activo || false;
    }
}
