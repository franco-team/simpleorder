import { Moment } from 'moment';

export const enum SEXO {
    MUJER = 'MUJER',
    HOMBRE = 'HOMBRE'
}

export interface ICliente {
    id?: number;
    nombre?: string;
    sexo?: SEXO;
    documento?: string;
    email?: string;
    telefono?: string;
    fecNacimiento?: Moment;
}

export class Cliente implements ICliente {
    constructor(
        public id?: number,
        public nombre?: string,
        public sexo?: SEXO,
        public documento?: string,
        public email?: string,
        public telefono?: string,
        public fecNacimiento?: Moment
    ) {}
}
