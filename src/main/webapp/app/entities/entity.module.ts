import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'producto',
                loadChildren: './producto/producto.module#SimpleordenProductoModule'
            },
            {
                path: 'cliente',
                loadChildren: './cliente/cliente.module#SimpleordenClienteModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SimpleordenEntityModule {}
